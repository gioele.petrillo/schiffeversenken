package battleship;

import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.Label;

import java.awt.*;

public class WindowsController {

    private static Stage primaryStage;
    private static Scene menu;
    private static Label labelShow;
    private static int zellen = 10;
    private static int schiffe = 5;
    private static Label schiffeUebrigLabel = new Label();


    public static int getSchiffe() {
        return schiffe;
    }

    public static void setSchiffe(int schiffee) {
        WindowsController.schiffe = schiffee;
        getSchiffeUebrigLabel().setText("Schiffe zu setzten: " + schiffe);
    }

    public static int getZellen() {
        return zellen;
    }

    public static Label getSchiffeUebrigLabel() {

        //Die Schriftart des Labels wird gesetzt und die Farbe
        schiffeUebrigLabel.setFont(new Font("Agency FB", 30));
        schiffeUebrigLabel.setTextFill(Color.web("#ffffff"));
        return schiffeUebrigLabel;
    }

    public static void setZellen(int zellen) {
        WindowsController.zellen = zellen;
    }

    public static void setPrimaryStage(Stage stage) {
        primaryStage = stage;
    }

    public static void setView(Scene scene){
        primaryStage.setScene(scene);
    }

    public static void openMenu(){
        primaryStage.setScene(menu);
    }

    public static void setMenu(Scene scene){
        menu = scene;
    }

    public static Label getLabelShow() {
        return labelShow;
    }

    public static void setLabelShow(Label labelToShow) {
       labelShow = labelToShow;
    }
}
