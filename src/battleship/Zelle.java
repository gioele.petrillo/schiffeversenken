package battleship;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Zelle extends Rectangle {
    public int x, y;
    public Ship ship = null;
    public boolean wasShot = false;

    private Board board;

    public Zelle(int x, int y, Board board) {
        //Einstellung der Grösse der Spielfelder
        super(40, 40);
        this.x = x;
        this.y = y;
        this.board = board;
        //Farbe der Spielbretter (Hauptfarbe/Randfarbe)
        setFill(Color.WHITE);
        setStroke(Color.BLACK);
    }

    public boolean shoot() {
        wasShot = true;
        //Farbe wenn man ins Leere trifft
        setFill(Color.LIGHTBLUE);

        // Wenn das Schiff getroffen wurde ändert sich die Farbe
        if (ship != null) {
            ship.hit();
            //Farbe wenn man ein Schiff trifft
            setFill(Color.RED);
            if (!ship.isAlive()) {
                board.ships--;
            }
            return true;
        }

        return false;
    }
}
