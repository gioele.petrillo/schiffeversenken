package battleship;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Random;

public class Content {
    private boolean running = false;
    private Board enemyBoard, playerBoard;

    private boolean enemyTurn = false;

    private int schiffe = WindowsController.getSchiffe();

    private Random random = new Random();

    //Erstelle Spielfeld und setze Grösse
    public Parent createContent() {

        //BorderPane Objekt wird erstellt
        BorderPane root = new BorderPane();
        root.setId("battleship");

        //Das Label wird erstellt
        Label labelSetz = new Label("Setzen Sie ihre Schiffe");

        //Label wird gesetzt
        WindowsController.setLabelShow(labelSetz);

        //Labels werden erstelt und die Schriftart geändert
        Label linksklick = new Label("Linksklick um Schiffe senkrecht zu setzten");
        linksklick.setFont(new Font("Agency FB", 20));
        linksklick.setTextFill(Color.web("#ffffff"));
        labelSetz.setTextFill(Color.web("#ffffff"));
        labelSetz.setFont(new Font("Agency FB", 30));

        //Es wird ein weiteres Label erstellt und die schrift gesetzt
        Label rechtsklick = new Label("Rechtsklick um Schiffe Waagrecht zu setzten");
        rechtsklick.setFont(new Font("Agency FB", 20));
        rechtsklick.setTextFill(Color.web("#ffffff"));

        Button menu = new Button("Zurück zum Menu");
        root.setPrefSize(1000, 600);

        //Erstelle Spielbrett des Gegners
        enemyBoard = new Board(true, event -> {
            if (!running)
                return;

            Zelle zelle = (Zelle) event.getSource();
            if (zelle.wasShot)
                return;

            enemyTurn = !zelle.shoot();

            //Bei Sieg erscheint eine Messagebox
            if (enemyBoard.ships == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Battleship Resultat");
                alert.setHeaderText(null);
                alert.setContentText("DU HAST GEWONNEN, WEITER SO!!!");


                alert.showAndWait();

                WindowsController.openMenu();
            }


            if (enemyTurn)
                enemyMove();
        });

        playerBoard = new Board(false, event -> {
            if (running)
                return;

            Zelle zelle = (Zelle) event.getSource();
            if (playerBoard.placeShip(new Ship(schiffe, event.getButton() == MouseButton.PRIMARY), zelle.x, zelle.y)) {
                if (--schiffe == 0) {
                    startGame();
                }
                WindowsController.getSchiffeUebrigLabel().setText("Schiffe zu setzten: " + schiffe);
            }
        });

        //Es wird das label formatiert
        VBox vBox = new VBox(50, WindowsController.getLabelShow(), linksklick, playerBoard);
        VBox vBox2 = new VBox(50, WindowsController.getSchiffeUebrigLabel(), rechtsklick, enemyBoard);
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox2.setAlignment(Pos.CENTER_LEFT);

        HBox ende = new HBox();
        ende.getChildren().add(vBox);
        ende.getChildren().add(vBox2);
        ende.setAlignment(Pos.BOTTOM_CENTER);
        ende.setSpacing(40);
        root.setCenter(ende);



        return root;
    }
    //Bot kann zufällig angreifen nach jedem Angriff vom Spieler
    private void enemyMove() {
        while (enemyTurn) {
            int x = random.nextInt(WindowsController.getZellen());
            int y = random.nextInt(WindowsController.getZellen());

            Zelle zelle = playerBoard.getCell(x, y);
            if (!zelle.wasShot) {

                enemyTurn = zelle.shoot();

                //Bei Niederlage erscheint eine Messagebox
                if (playerBoard.ships == 0) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Battleship Resultat");
                    alert.setHeaderText(null);
                    alert.setContentText("LEIDER HAST DU VERLOREN!");
                    alert.showAndWait();

                    //Es wird wieder der Startbildschirm angezeigt
                    WindowsController.openMenu();
                }
            }
        }
    }
    //Spiel wird gestartet
    private void startGame() {

        WindowsController.getLabelShow().setText("Kämpfen Sie jetzt!!!");
        //Gegner Schiffe werden gesetzt
        int type = WindowsController.getSchiffe();

        while (type > 0) {
            int x = random.nextInt(WindowsController.getZellen());
            int y = random.nextInt(WindowsController.getZellen());

            if (enemyBoard.placeShip(new Ship(type, Math.random() < 0.5), x, y)) {
                type--;
            }
        }
        running = true;


    }


}


