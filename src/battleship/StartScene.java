package battleship;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class  StartScene{


    public Scene beginn(Stage primaryStage) {

        //Neues Borderpane
        BorderPane borderPane = new BorderPane();

        //ID für CSS vergeben
        borderPane.setId("pane");

        //Neue Scene erstellen
        Scene startScene = new Scene(borderPane, 800, 500);

        //Verlinkung des Stylesheets "style.css"
        startScene.getStylesheets().add(StartScene.class.getResource("style.css").toExternalForm());

        //Schiffeversenken Titel erstellen durch Label
        Label titel = new Label("Schiffeversenken");

        //Titel zur BorderPane hinzufügen mit Positionsänderungen
        titel.getStyleClass().add("titel");
        borderPane.setTop(titel);
        BorderPane.setAlignment(titel, Pos.TOP_CENTER);
        borderPane.setMargin(titel, new Insets(30, 0, 0, 0));

        //Spiel starten Button erstellen
        Button startSpiel = new Button();
        startSpiel.setText("Schiffe anordnen");
        startSpiel.setWrapText(true);

        // Einstellungen Button
        Button einstellungen = new Button("Einstellungen");
        einstellungen.setWrapText(true);

        //Spiel beenden Button erstellen
        Button spielBeenden = new Button("Spiel beenden");
        spielBeenden.setWrapText(true);

        einstellungen.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                Einstellungen eins = new Einstellungen();
                primaryStage.setScene(eins.einstellungen());
            }
        });

        // Fenster schliessen
        spielBeenden.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.close();
            }});

        //Wenn der Knopf gedrückt wird
        startSpiel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(WindowsController.getZellen() == 15){
                    WindowsController.setSchiffe(7);
                }else if(WindowsController.getZellen() == 12){
                    WindowsController.setSchiffe(6);
                }else{
                    WindowsController.setSchiffe(5);
                }
                Content content = new Content();
                Scene scene = new Scene(content.createContent());

                //Verlinkung des Stylesheets "battleship.css"
                scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

                WindowsController.setView(scene);

            }
        });

        VBox knoepfe = new VBox();

        //Add startSpiel button to scene
        knoepfe.getChildren().add(startSpiel);
        knoepfe.getChildren().add(einstellungen);
        knoepfe.getChildren().add(spielBeenden);
        knoepfe.setSpacing(10);

        knoepfe.setAlignment(Pos.BOTTOM_CENTER);
        borderPane.setBottom(knoepfe);


        WindowsController.setMenu(startScene);
        return startScene;
    }
}