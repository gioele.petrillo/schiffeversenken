package battleship;


import java.util.ArrayList;
import java.util.List;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Board extends Parent {
    //Grid wird erstellt und es wird Anzahl Schiffe des Spels angegeben
    private VBox rows = new VBox();
    private boolean enemy = false;
    public int ships = WindowsController.getSchiffe();

    //Das Brett, des Feindes wird erstellt
    public Board(boolean enemy, EventHandler<? super MouseEvent> handler) {
        this.enemy = enemy;

        //Es wird eine Reihe mit x Spalten erstellt
        for (int y = 0; y < WindowsController.getZellen(); y++) {
            //Es wird eine neue Hbox erstell, dies ist die Reihe
            HBox row = new HBox();
            row.setSpacing(2);
            //Es werden x Zellen erstellt
            for (int x = 0; x < WindowsController.getZellen(); x++) {
                Zelle c = new Zelle(x, y, this);
                c.setOnMouseClicked(handler);
                row.getChildren().add(c);
            }

            rows.getChildren().add(row);
            rows.setSpacing(2);
        }
        getChildren().add(rows);
    }

    //Zum Schiffe setzen werden x gebraucht welche der Grösse nach x immer kleiner werden (nacheinander)
    public boolean placeShip(Ship ship, int x, int y) {
        if (canPlaceShip(ship, x, y)) {
            int length = ship.type;

            //Vertikale Schiffe
            if (ship.vertical) {
                for (int i = y; i < y + length; i++) {
                    Zelle zelle = getCell(x, i);
                    zelle.ship = ship;
                    if (!enemy) {
                        zelle.setFill(Color.BLACK);
                        zelle.setStroke(Color.BLACK);
                    }
                }
            }
            //Horizontale Schiffe
            else {
                for (int i = x; i < x + length; i++) {
                    Zelle zelle = getCell(i, y);
                    zelle.ship = ship;
                    if (!enemy) {
                        zelle.setFill(Color.BLACK);
                        zelle.setStroke(Color.BLACK);
                    }
                }
            }

            return true;
        }

        return false;
    }



    public Zelle getCell(int x, int y) {
        return (Zelle)((HBox)rows.getChildren().get(y)).getChildren().get(x);
    }

    //Schiffe werden so gesetzt (Bei Klick die Nachbarpunkte verbinden)
    private Zelle[] getNeighbors(int x, int y) {
        Point2D[] points = new Point2D[] {
                new Point2D(x - 1, y),
                new Point2D(x + 1, y),
                new Point2D(x, y - 1),
                new Point2D(x, y + 1)
        };

        List<Zelle> neighbors = new ArrayList<Zelle>();

        for (Point2D p : points) {
            if (isValidPoint(p)) {
                neighbors.add(getCell((int)p.getX(), (int)p.getY()));
            }
        }

        return neighbors.toArray(new Zelle[0]);
    }

    private boolean canPlaceShip(Ship ship, int x, int y) {
        int length = ship.type;

        if (ship.vertical) {
            for (int i = y; i < y + length; i++) {
                if (!isValidPoint(x, i))
                    return false;

                Zelle zelle = getCell(x, i);
                if (zelle.ship != null)
                    return false;

                for (Zelle neighbor : getNeighbors(x, i)) {
                    if (!isValidPoint(x, i))
                        return false;

                    if (neighbor.ship != null)
                        return false;
                }
            }
        }
        else {
            for (int i = x; i < x + length; i++) {
                if (!isValidPoint(i, y))
                    return false;

                Zelle zelle = getCell(i, y);
                if (zelle.ship != null)
                    return false;

                for (Zelle neighbor : getNeighbors(i, y)) {
                    if (!isValidPoint(i, y))
                        return false;

                    if (neighbor.ship != null)
                        return false;
                }
            }
        }

        return true;
    }

    private boolean isValidPoint(Point2D point) {
        return isValidPoint(point.getX(), point.getY());
    }

    private boolean isValidPoint(double x, double y) {
        return x >= 0 && x < WindowsController.getZellen() && y >= 0 && y < WindowsController.getZellen();
    }

}