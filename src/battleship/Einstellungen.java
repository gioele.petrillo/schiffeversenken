package battleship;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class Einstellungen {

    public Scene einstellungen(){


        //Borderpane wird erstellt
        BorderPane bp = new BorderPane();
        bp.setId("einstellungen");

        //Der Button, für 10 x 10 wird erstellt
        Button ten = new Button("10 X 10");

        ten.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                WindowsController.setZellen(10);
            }
        });

        //Der Button für 15 x 15 wird erstellt
        Button fifteen = new Button("12 x 12");

        fifteen.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                WindowsController.setZellen(12);
            }
        });
        //Der Button für 20 x 20 wird erstellt
        Button twenty = new Button("15 x 15");

        twenty.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                WindowsController.setZellen(15);

            }
        });
        //Hbox wird erstellt
        HBox hb = new HBox();

        //Die Buttons werden in die HBox eingefügt
        hb.getChildren().add(ten);
        hb.getChildren().add(fifteen);
        hb.getChildren().add(twenty);

        hb.setSpacing(10);

        //Borderpane für die HBox wird gesetzt
        hb.setAlignment(Pos.CENTER);
        hb.setMargin(hb, new Insets(12,12,12,12));

        //Die HBox wird in das Borderpane eingefügt
        bp.setCenter(hb);

        // Button um zurück zum Menü zu gelangen
        Button menu = new Button("Menu");
        BorderPane.setAlignment(menu, Pos.BOTTOM_CENTER);
        bp.setBottom(menu);

        menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                WindowsController.openMenu();
            }
        });

        Scene einstellungen = new Scene(bp, 300, 300);


        //Verlinkung des Stylesheets "style.css"
        einstellungen.getStylesheets().add(Einstellungen.class.getResource("style.css").toExternalForm());

        return einstellungen;
    }
}
