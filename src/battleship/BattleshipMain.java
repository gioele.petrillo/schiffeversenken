package battleship;

import javafx.application.Application;
import javafx.stage.Stage;

public class BattleshipMain extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        WindowsController.setPrimaryStage(primaryStage);
        StartScene startScene = new StartScene();
        primaryStage.setTitle("Schiffeversenken");
        primaryStage.setScene(startScene.beginn(primaryStage));
        primaryStage.setMinHeight(500);
        primaryStage.setMinWidth(800);
        primaryStage.show();


    }

    public static void main(String[] args) {
        launch(args);
    }
}
